'use strict';

const uuid = require('uuid'),
  Transaction = require('./models/transaction');

// userId, itemId, qty
exports.buy = function(record) {
  return new Promise(function(resolve, reject) {
    record.id = uuid.v4();
    Transaction.create(record, function(err, res) {
      if (err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
};