'use strict';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const transactionSchema = new Schema({
  id: String,
  orderNo: String,
  userId: String,
  itemId: String,
  qty: Number
});

module.exports = mongoose.model('Transaction', transactionSchema);