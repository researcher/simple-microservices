'use strict';

const manager = require('./manager');

const messanger = function(amqpConnection) {
  return {
    publish: publish(amqpConnection),
    consume: consume(amqpConnection),
  };
};
exports.messanger = messanger;

const publish = function(amqpConnection) {
  // not implemented
};
exports.publish = publish;

const consume = function(amqpConnection) {
  return {
    buy: function() {
      const queue = 'sales:buy';
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              channel.consume(queue, function(msg) {
                if (msg !== null) {
                  const buyingInformation = JSON.parse(msg.content.toString());
                  manager.buy(buyingInformation);
                }

                channel.ack(msg);
              });
            });
        });
    },
  };
};
exports.consume = consume;