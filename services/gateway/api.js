'use strict';

function Api(amqpConnection, server) {
  this.amqpConnection = amqpConnection;
  this.server = server;
}

Api.prototype.index = function() {
  const me = this;

  me.server.get('/', function(req, res, next) {
    res.json(200, { status: 'running' });
    return next();
  });
};

Api.prototype.registerUser = function() {
  const me = this;

  me.server.post('/ums/user', function(req, res, next) {
    const queue = 'ums:registerUser';
    me.amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const payload = new Buffer(JSON.stringify(req.body.payload));
            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            res.json(200, { acknowledged });
            return next();
          });
      })
      .catch(function(error) {
        res.json(400, { error });
      });
  });
};

// FIXME: should bind to callback route
Api.prototype.searchUser = function() {
  const me = this;

  me.server.get('/ums/user/:id', function(req, res, next) {
    const queue = 'ums:lookupUser';
    me.amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const payload = new Buffer(req.params.id);
            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            res.json(200, { acknowledged });
            return next();
          });
      })
      .catch(function(error) {
        res.json(400, { error });
        return next();
      })
  });
};

Api.prototype.buy = function() {
  const me = this;

  me.server.post('/sales/buy', function(req, res, next) {
    const queue = 'sales:buy';
    me.amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const payload = new Buffer(JSON.stringify(req.body.payload));
            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            res.json(200, { acknowledged });
            return next();
          });
      })
      .catch(function(error) {
        res.json(400, { error });
        return next();
      });
  });
};

Api.prototype.registerItem = function() {
  const me = this;

  me.server.post('/inventory/item', function(req, res, next) {
    const queue = 'inv:registerItem';
    me.amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const payload = new Buffer(JSON.stringify(req.body.payload));
            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            res.json(200, { acknowledged });
            return next();
          });
      })
      .catch(function(error) {
        console.log(error);
        res.json(400, { error });
        return next();
      });
  });
};

// FIXME: should bind to callback route
Api.prototype.searchItem = function() {
  const me = this;
  
  me.server.get('/inventory/item/:id', function(req, res, next) {
    const queue = 'inv:lookupItem';
    me.amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const payload = new Buffer(JSON.stringify(req.body.payload));
            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            res.json(200, { acknowledged });
            return next();
          });
      })
      .catch(function(error) {
        res.json(400, { error });
        return next();
      });
  });
};

module.exports = Api;