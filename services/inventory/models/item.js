'use strict';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const itemSchema = new Schema({
  id: String,
  name: String,
  qty: Number,
  price: Number
});

module.exports = mongoose.model('Item', itemSchema);