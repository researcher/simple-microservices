'use strict';

const manager = require('./manager');

const messanger = function(amqpConnection) {
  return {
    publish: publish(amqpConnection),
    consume: consume(amqpConnection),
  };
};
exports.messanger = messanger;

const publish = function(amqpConnection) {
  return {
    itemById: function(id) {
      const queue = 'inv:itemById:' + id;
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              return manager.searchById(id)
                .then(function(item) {
                  const payload = new Buffer(JSON.stringify(item));
                  return channel.sendToQueue(queue, payload);
                });
            });
        });
    },
  };
};
exports.publish = publish;

const consume = function(amqpConnection) {
  return {
    lookupItem: function() {
      const queue = 'inv:lookupItem'
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              channel.consume(queue, function(msg) {
                if (msg !== null) {
                  const id = msg.content.toString();
                  messanger(amqpConnection).publish.itemById(id)
                }

                channel.ack(msg);
              });
            });
        });
    },
    useItem: function() {
      const queue = 'inv:useItem';
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              channel.consume(queue, function(msg) {
                if (msg !== null) {
                  const itemUsage = JSON.parse(msg.content.toString());
                  manager.use(itemUsage.id, itemUsage.qty);
                }

                channel.ack(msg);
              });
            });
        });
    },
    registerItem: function() {
      const queue = 'inv:registerItem';
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              channel.consume(queue, function(msg) {
                if (msg !== null) {
                  const item = JSON.parse(msg.content.toString());
                  manager.register(item)
                }

                channel.ack(msg);
              });
            });
        });
    },
  }
};
exports.consume = consume;