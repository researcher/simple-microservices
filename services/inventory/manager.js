'use strict';

const uuid = require('uuid'),
  Item = require('./models/item');

exports.register = function(record) {
  return new Promise(function(resolve, reject) {
    record.id = uuid.v4();
    Item.create(record, function(err, res) {
      if (err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
};

exports.use = function(id, qty) {
  return new Promise(function(resolve, reject) {
    Item.where({ id }).exec(function(errFind, resFind) {
      if (errFind) {
        return reject(errFind);
      }

      Item.update({
        qty: (resFind[0].qty - qty) 
      })
      .where({ id }).exec(function(err, res) {
        if (err) {
          return reject(err);
        }

        return resolve(res);
      });
    });
  })
};

exports.searchById = function(id) {
  return new Promise(function(resolve, reject) {
    Item.where({ id }).exec(function(err, res) {
      if (err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
};