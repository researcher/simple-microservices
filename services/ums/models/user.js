'use strict';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const userSchema = new Schema({
  id: String,
  fullname: String,
  balance: Number
});

module.exports = mongoose.model('User', userSchema);