'use strict';

const uuid = require('uuid'),
  User = require('./models/user');

exports.register = function(record) {
  return new Promise(function(resolve, reject) {
    record.id = uuid.v4();
    User.create(record, function(err, res) {
      if (err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
};

exports.searchById = function(id) {
  return new Promise(function(resolve, reject) {
    User.where({ id }).exec(function(err, res) {
      if (err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
};