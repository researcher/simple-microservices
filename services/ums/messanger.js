'use strict';

const manager = require('./manager');

const messanger = function(amqpConnection) {
  return {
    publish: publish(amqpConnection),
    consume: consume(amqpConnection),
  }
};
exports.messanger = messanger;

const publish = function(amqpConnection) {
  return {
    userById: function(id) {
      const queue = 'ums:userById:' + id;
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              return manager.searchById(id)
                .then(function(user) {
                  const payload = new Buffer(JSON.stringify(user));
                  return channel.sendToQueue(queue, payload);
                })
            });
        });
    },
  }
};
exports.publish = publish;

const consume = function(amqpConnection) {
  return {
    lookupUser: function() {
      const queue = 'ums:lookupUser'
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              channel.consume(queue, function(msg) {
                if (msg !== null) {
                  const id = msg.content.toString();
                  messanger(amqpConnection).publish.userById(id)
                }

                channel.ack(msg);
              });
            });
        });
    },
    registerUser: function() {
      const queue = 'ums:registerUser'
      amqpConnection
        .then(function(client) {
          return client.createChannel();
        })
        .then(function(channel) {
          return channel.assertQueue(queue)
            .then(function(ok) {
              channel.consume(queue, function(msg) {
                if (msg !== null) {
                  const user = JSON.parse(msg.content.toString());
                  manager.register(user)
                }

                channel.ack(msg);
              });
            });
        });
    },
  }
}
exports.consume = consume;