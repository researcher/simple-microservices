'use strict';

const amqplib = require('amqplib');

const open = amqplib.connect('amqp://localhost'),
  q = 'sample';

open
  .then(function(client) {
    return client.createChannel();
  })
  .then(function(channel) {
    return channel.assertQueue(q)
      .then(function(ok) {
        return channel.sendToQueue(q, new Buffer('Message to send'));
      });
  })
  .catch(console.warn);

open
  .then(function(client) {
    return client.createChannel();
  })
  .then(function(channel) {
    return channel.assertQueue(q)
      .then(function(ok) {
        return channel.consume(q, function(msg) {
          if (msg !== null) {
            console.log(msg);
            // console.log(msg.content.toString());
          }
          channel.ack(msg);
        });
      });
  })
  .catch(console.warn);