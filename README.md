#### ==== Simple Microservices ==== ####

Setup:

I assume you have node, npm, docker, and docker-compose installed on your environment.

If not, please navigate and follow the instruction to install them here:

- [NVM (node version manager)](https://github.com/creationix/nvm#installation)
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

Commands:
`````
npm install
`````

`````
docker-compose build
`````
To run services
`````
docker-compose up -d mongo && \
docker-compose up -d rabbit && \
docker-compose up -d ums && \
docker-compose up -d sales && \
docker-compose up -d gateway
`````
To run test:
`````
docker-compose up test
`````

