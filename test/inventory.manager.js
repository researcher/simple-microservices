'use strict';

const mongoose = require('mongoose'),
  faker = require('faker'),
  expect = require('chai').expect,
  manager = require('./../services/inventory/manager');

describe('inventory.manager', function() {
  before(function(done) {
    mongoose.connect('mongodb://mongo', done);
  });

  after(function(done) {
    mongoose.connection.close(done);
  });

  it('should save record', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    manager.register(record)
      .then(function(res) {
        expect({ id: res.id, name: res.name, qty: res.qty }).to.eql(record);
        done();
      })
      .catch(done);
  });

  it('expect find user by id', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    manager.register(record)
      .then(function(res) {
        return manager.searchById(res.id)
      })
      .then(function(res) {
        expect({ id: res[0].id, name: res[0].name, qty: res[0].qty }).to.eql(record);
        done();
      })
      .catch(done);
  });

  it('should use invetory item', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    let id;
    manager.register(record)
      .then(function(res) {
        id = res.id;
        return manager.use(res.id, 1);
      })
      .then(function(res) {
        return manager.searchById(id);
      })
      .then(function(res) {
        expect(res[0].qty).to.eql(record.qty - 1);
        done();
      })
      .catch(done);
  });

});