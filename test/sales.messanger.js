'use strict';

const mongoose = require('mongoose'),
  amqplib = require('amqplib'),
  faker = require('faker'),
  uuid = require('uuid'),
  expect = require('chai').expect,
  messanger = require('./../services/sales/messanger').messanger,
  manager = require('./../services/sales/manager');

describe('sales.messanger', function() {
  let amqpConnection;

  before(function(done) {
    mongoose.connect('mongodb://mongo', function() {
      amqpConnection = amqplib.connect('amqp://rabbit');
      done();
    });
  });

  after(function(done) {
    mongoose.connection.close(function() {
      amqpConnection
        .then(function(client) {
          client.close();
          done();
        });
    });
  });

  it('should ensure publisher and consumer as a function', function(done) {
    const consumer = messanger(amqpConnection).consume;

    expect(consumer).to.be.a('object');
    done();
  });

  it('should consume buy', function(done) {
    const consumer = messanger(amqpConnection).consume,
      queue = 'sales:buy';
    consumer.buy();
    
    amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const record = {
                userId: uuid.v4(),
                itemId: uuid.v4(),
                qty: Number(faker.finance.amount()),
              },
              payload = new Buffer(JSON.stringify(record));

            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            expect(acknowledged).to.be.ok;
            done();
          });
      });
  });
});