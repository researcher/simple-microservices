'use strict';

const mongoose = require('mongoose'),
  amqplib = require('amqplib'),
  faker = require('faker'),
  uuid = require('uuid'),
  expect = require('chai').expect,
  messanger = require('./../services/inventory/messanger').messanger,
  manager = require('./../services/inventory/manager');

describe('inventory.messanger', function() {
  let amqpConnection;

  before(function(done) {
    mongoose.connect('mongodb://mongo', function() {
      amqpConnection = amqplib.connect('amqp://rabbit');
      done();
    });
  });

  after(function(done) {
    mongoose.connection.close(function() {
      amqpConnection
        .then(function(client) {
          client.close();
          done();
        });
    });
  });

  it('should ensure publisher and consumer as a function', function(done) {
    const publisher = messanger(amqpConnection).publish,
      consumer = messanger(amqpConnection).consume;

    expect(publisher).to.be.a('object');
    expect(consumer).to.be.a('object');
    done();
  });

  it('should consume registration', function(done) {
    const consumer = messanger(amqpConnection).consume,
      queue = 'inv:registerItem';
    consumer.registerItem();
    
    amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const record = {
                name: faker.commerce.productName(),
                qty: Number(faker.finance.amount())
              },
              payload = new Buffer(JSON.stringify(record));

            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            expect(acknowledged).to.be.ok;
            done();
          });
      });
  });

  it('should consume usage of item', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    manager.register(record)
      .then(function(item) {
        const consumer = messanger(amqpConnection).consume,
              queue = 'inv:useItem';
            consumer.useItem();
            
          amqpConnection
            .then(function(client) {
              return client.createChannel();
            })
            .then(function(channel) {
              return channel.assertQueue(queue)
                .then(function(ok) {
                  const usage = {
                      id: item.id,
                      qty: 1
                    },
                    payload = new Buffer(JSON.stringify(usage));

                  return channel.sendToQueue(queue, payload);
                })
                .then(function(acknowledged) {
                  expect(acknowledged).to.be.ok;
                  done();
                });
            });
      });
  });

  

  it('should publish item by id', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    manager.register(record)
      .then(function(item) {
        const publisher = messanger(amqpConnection).publish,
          queue = 'inv:itemById:' + item.id;

        amqpConnection
          .then(function(client) {
            return client.createChannel();
          })
          .then(function(channel) {
            return channel.assertQueue(queue)
              .then(function(ok) {
                channel.consume(queue, function(msg) {
                  expect(msg).to.be.ok;
                  const payload = JSON.parse(msg.content.toString());
                  expect(payload[0].id).to.eql(item.id);
                  done();
                });
            });
          });

        publisher.itemById(item.id);
      });
  });

  it('should consume lookup item', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    manager.register(record)
      .then(function(item) {
        const consumer = messanger(amqpConnection).consume,
          queue = 'inv:lookupItem';
        consumer.lookupItem();
        
        amqpConnection
          .then(function(client) {
            return client.createChannel();
          })
          .then(function(channel) {
            return channel.assertQueue(queue)
              .then(function(ok) {
                const payload = new Buffer(JSON.stringify(item.id));

                return channel.sendToQueue(queue, payload);
              })
              .then(function(acknowledged) {
                expect(acknowledged).to.be.ok;
                done();
              });
          });
      });
  });
});