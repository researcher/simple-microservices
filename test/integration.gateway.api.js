'use strict';

const request = require('supertest-as-promised'),
  faker = require('faker'),
  uuid = require('uuid'),
  expect = require('chai').expect;

describe('integration.gateway.api', function() {
  const base = 'http://gateway:8099';

  it('should return status running', function(done) {
    request.agent(base)
      .get('/')
      .expect(200)
      .then(function(res) {
        expect(res.body.status).to.eql('running');
        done();
      })
      .catch(done);
  });

  it('should register user', function(done) {
    const fullname = faker.name.findName();

    request.agent(base)
      .post('/ums/user')
      .send({ payload: { fullname } })
      .expect(200)
      .then(function(res) {
        expect(res.body.acknowledged).to.eql(true);
        done();
      })
      .catch(done);
  });

  it.skip('should search user by id', function(done) {
    // expression
  });

  it('should buy an item', function(done) {
    const record = {
      userId: uuid.v4(),
      itemId: uuid.v4(),
      qty: Number(faker.finance.amount()),
    };

    request.agent(base)
      .post('/sales/buy')
      .send({ payload: { record } })
      .expect(200)
      .then(function(res) {
        expect(res.body.acknowledged).to.eql(true);
        done();
      })
      .catch(done);
  });

  it('should register item', function(done) {
    const record = {
      name: faker.commerce.productName(),
      qty: Number(faker.finance.amount())
    };

    request.agent(base)
      .post('/inventory/item')
      .send({ payload: { record } })
      .expect(200)
      .then(function(res) {
        expect(res.body.acknowledged).to.eql(true);
        done();
      })
      .catch(done);
  });

  it.skip('should search item by id', function(done) {
    // expression
  });
});