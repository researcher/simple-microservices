'use strict';

const mongoose = require('mongoose'),
  amqplib = require('amqplib'),
  faker = require('faker'),
  uuid = require('uuid'),
  expect = require('chai').expect,
  messanger = require('./../services/ums/messanger').messanger,
  manager = require('./../services/ums/manager');

describe('ums.messanger', function() {
  let amqpConnection;

  before(function(done) {
    mongoose.connect('mongodb://mongo', function() {
      amqpConnection = amqplib.connect('amqp://rabbit');
      done();
    });
  });

  after(function(done) {
    mongoose.connection.close(function() {
      amqpConnection
        .then(function(client) {
          client.close();
          done();
        });
    });
  });

  it('should ensure publisher and consumer as a function', function(done) {
    const publisher = messanger(amqpConnection).publish,
      consumer = messanger(amqpConnection).consume;

    expect(publisher).to.be.a('object');
    expect(consumer).to.be.a('object');
    done();
  });

  it('should consume registration', function(done) {
    const consumer = messanger(amqpConnection).consume,
      queue = 'ums:registerUser';
    consumer.registerUser();
    
    amqpConnection
      .then(function(client) {
        return client.createChannel();
      })
      .then(function(channel) {
        return channel.assertQueue(queue)
          .then(function(ok) {
            const fullname = faker.name.findName(),
              payload = new Buffer(JSON.stringify({ fullname }));

            return channel.sendToQueue(queue, payload);
          })
          .then(function(acknowledged) {
            expect(acknowledged).to.be.ok;
            done();
          });
      });
  });

  it('should publish user by id', function(done) {
    const fullname = faker.name.findName();

    manager.register({ fullname })
      .then(function(user) {
        const publisher = messanger(amqpConnection).publish,
          queue = 'ums:userById:' + user.id;

        amqpConnection
          .then(function(client) {
            return client.createChannel();
          })
          .then(function(channel) {
            return channel.assertQueue(queue)
              .then(function(ok) {
                channel.consume(queue, function(msg) {
                  expect(msg).to.be.ok;
                  const payload = JSON.parse(msg.content.toString());
                  expect(payload[0].id).to.eql(user.id);
                  done();
                });
            });
          });

        publisher.userById(user.id);
      });
  });

  it('should consume lookup user', function(done) {
    const fullname = faker.name.findName();

    manager.register({ fullname })
      .then(function(user) {
        const consumer = messanger(amqpConnection).consume,
          queue = 'ums:lookupUser';
        consumer.lookupUser();
        
        amqpConnection
          .then(function(client) {
            return client.createChannel();
          })
          .then(function(channel) {
            return channel.assertQueue(queue)
              .then(function(ok) {
                const payload = new Buffer(JSON.stringify(user.id));

                return channel.sendToQueue(queue, payload);
              })
              .then(function(acknowledged) {
                expect(acknowledged).to.be.ok;
                done();
              });
          });
      });
  });
});