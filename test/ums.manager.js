'use strict';

const mongoose = require('mongoose'),
  faker = require('faker'),
  expect = require('chai').expect,
  manager = require('./../services/ums/manager');

describe('ums.manager', function() {
  before(function(done) {
    mongoose.connect('mongodb://mongo', done);
  });

  after(function(done) {
    mongoose.connection.close(done);
  });

  it('expect save record', function(done) {
    const fullname = faker.name.findName();
    manager.register({ fullname })
      .then(function(res) {
        expect(res.fullname).to.eql(fullname);
        done();
      })
      .catch(done)
  });

  it('expect find user by id', function(done) {
    const fullname = faker.name.findName();
    manager.register({ fullname })
      .then(function(res) {
        return manager.searchById(res.id)
      })
      .then(function(res) {
        expect(res[0].fullname).to.eql(fullname);
        done();
      })
      .catch(done);
  });
});