'use strict';

const mongoose = require('mongoose'),
  faker = require('faker'),
  uuid = require('uuid'),
  expect = require('chai').expect,
  manager = require('./../services/sales/manager');

describe('sales.manager', function() {
  before(function(done) {
    mongoose.connect('mongodb://mongo', done);
  });

  after(function(done) {
    mongoose.connection.close(done);
  });

  it('should save record', function(done) {
    const record = {
      userId: uuid.v4(),
      itemId: uuid.v4(),
      qty: Number(faker.finance.amount()),
    };

    manager.buy(record)
      .then(function(res) {
        expect({
          id: res.id,
          userId: res.userId,
          itemId: res.itemId,
          qty: res.qty
        }).to.eql(record);
        done();
      })
      .catch(done);
  });

});